ruby version is 2.3.1

database is sqlite

database creation :
bin/rails db:create
bin/rails db:migrate
bin/rails db:seed

delete dabases (if needed) :
bin/rails db:drop

run tests with :
bin/rails test


Front End is using angular v1.5.8 and material design (https://getmdl.io/)

The delivery charge rules, the catalog and the special offer are stored in the DB and are seeded from the seeds.rb file.

The logic happens in the model classes: Basket, Offer, OfferBuyOneGetOneHalfPrice, DeliveryChargeRule

OfferBuyOneGetOneHalfPrice inherits from Offer. So we can have different types of offers


Docker
You can run this app with docker:
docker-compose build
docker-compose up
