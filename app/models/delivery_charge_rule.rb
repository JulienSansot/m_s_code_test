class DeliveryChargeRule < ApplicationRecord

  def self.get_charge(delivery_charge_rules, basket_total_price)
    rule_to_apply = delivery_charge_rules
                      .sort_by(&:basket_price_threshold)
                      .select{|rule| rule.basket_price_threshold > basket_total_price}
                      .first

    if rule_to_apply != nil
      rule_to_apply.delivery_charge
    else
      0
    end
  end

end
