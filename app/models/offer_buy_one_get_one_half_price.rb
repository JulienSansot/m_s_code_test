class OfferBuyOneGetOneHalfPrice < Offer

  # logic specific to this type of offer
  # if there are at least two products matching the offer, the second one is half price
  def getOfferDiscount(products)

    matching_products = products.select{|product|
      product.code == self.product_code
    }

    if matching_products.size > 1
      matching_products[1].price / 2
    else
      0
    end
    
  end

end
