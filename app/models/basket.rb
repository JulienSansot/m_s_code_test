class Basket

  attr_reader :products

  def initialize(product_catalog = [], delivery_charge_rules = [], offers = [])
    @product_catalog = product_catalog
    @delivery_charge_rules = delivery_charge_rules
    @offers = offers
    @products = []
  end

  def add(product_code)
    product = @product_catalog.find{ |product|
      product.code == product_code
    }

    if product != nil
      @products << product
    end
  end

  def remove(product_code)
    product_index = @products.find_index{|product|
      product.code == product_code
    }

    if product_index != nil
      @products.delete_at(product_index)
    end
  end

  def total
    sum_products_price = 0
    @products.each{|product|
      sum_products_price += product.price
    }

    # getting the discounts from special offers
    offer_discount = Offer.GetOfferDiscounts(@offers, @products)

    basket_total = sum_products_price - offer_discount

    # adding delivery charge
    delivery_charge = DeliveryChargeRule.get_charge(@delivery_charge_rules, basket_total)

    basket_total += delivery_charge
    
    basket_total
  end

end
