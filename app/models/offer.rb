class Offer < ApplicationRecord

  def self.GetOfferDiscounts(offers, products)
    discount = 0

    offers_to_apply = offers.select{|offer|
      products.map(&:code).include?(offer.product_code)
    }

    offers_to_apply.each{|offer|
      discount += offer.getOfferDiscount(products)
    }

    discount
  end

end
