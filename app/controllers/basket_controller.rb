class BasketController < ApplicationController

  def summary
    products = user_basket.products.map{|product|
      {
        name: product.name,
        price: product.price
      }
    }

    render json: {
      products: products,
      total: user_basket.total
    }
  end

  def add_product
    product_code = params[:product_code]

    user_basket.add(product_code)

    render json: { status: 'ok'}
  end

  def remove_product
    product_code = params[:product_code]

    user_basket.remove(product_code)

    render json: { status: 'ok'}
  end

  private

    #
    # TODO: the basket should be stored in session
    #
    def user_basket
      if $basket == nil
        $basket = Basket.new(Product.all.to_a, DeliveryChargeRule.all.to_a, Offer.all.to_a)
      end

      $basket
    end

end
