angular.module('shop').controller("basket_ctrl",
['$scope', '$http', function($scope, $http){

  $scope.products = [];
  $scope.total = 0;

  fetchBasketSummary();

  function fetchBasketSummary(){
    $http.get('basket/summary')
    .success(function(result) {
      $scope.products = result.products;
      $scope.total = Math.floor(result.total * 100) / 100;
    });
  }

}]);