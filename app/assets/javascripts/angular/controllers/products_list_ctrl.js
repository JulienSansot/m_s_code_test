angular.module('shop').controller("products_list_ctrl",
['$scope', '$http', function($scope, $http){

  $scope.products = [];

  var snackbarContainer = document.querySelector('#snackbar-container');

  fetchProducts();

  $scope.addToBasket = function(product){
    $http.post('basket/add_product', {
      product_code: product.code
    })
    .success(function(result) {
      snackbarContainer.MaterialSnackbar.showSnackbar({
        timeout: 1000,
        message: product.name + ' added to basket'
      });
    });
  }

  $scope.removeFromBasket = function(product){
    $http.post('basket/remove_product', {
      product_code: product.code
    })
    .success(function(result) {
      snackbarContainer.MaterialSnackbar.showSnackbar({
        timeout: 1000,
        message: product.name + ' removed from basket'
      });
    });
  }

  function fetchProducts(){
    $http.get('products')
    .success(function(result) {
      $scope.products = result.products;
    });
  }

}]);