

var Angular_app = angular.module('shop', ['ngRoute'])
  .config(['$routeProvider', '$locationProvider', '$httpProvider',
    function ($routeProvider, $locationProvider, $httpProvider) {
    $routeProvider
      .when('/', { templateUrl: 'products_list', controller: 'products_list_ctrl' })
      .when('/basket', { templateUrl: 'basket', controller: 'basket_ctrl' })
      .otherwise({ redirectTo: '/' });

    $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');

    $locationProvider
    .html5Mode(false);

  }]);
  