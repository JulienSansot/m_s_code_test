FROM rails:5.0.0

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN gem install bundler

COPY Gemfile /usr/src/app/
COPY Gemfile.lock /usr/src/app/
RUN bundle install
COPY . /usr/src/app

RUN bundle exec rake assets:precompile

EXPOSE 3000

ENTRYPOINT bundle exec rake db:create db:migrate db:seed && \
           bundle exec rails s -p 3000 -b '0.0.0.0'