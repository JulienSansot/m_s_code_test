# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Product.create(name: 'Jeans',  code: 'J01', price: 32.95, image_url: 'http://asset1.marksandspencer.com/is/image/mands/SD_01_T57_6349_GJ_X_EC_0?$PDP_PROD_IMAGE$')
Product.create(name: 'Blouse', code: 'B01', price: 24.95, image_url: 'http://asset1.marksandspencer.com/is/image/mands/SD_01_T43_5611_P6_X_EC_0?$PDP_PROD_IMAGE$')
Product.create(name: 'Socks',  code: 'S01', price: 7.95,  image_url: 'http://asset1.marksandspencer.com/is/image/mands/HT_03_T10_9610_Y4_X_EC_0?$PDP_PROD_IMAGE$')

DeliveryChargeRule.create(basket_price_threshold: 50, delivery_charge: 4.95)
DeliveryChargeRule.create(basket_price_threshold: 90, delivery_charge: 2.95)

Offer.create(type: 'OfferBuyOneGetOneHalfPrice', product_code: 'J01')