class CreateDeliveryChargeRules < ActiveRecord::Migration[5.0]
  def change
    create_table :delivery_charge_rules do |t|
      t.decimal :basket_price_threshold
      t.decimal :delivery_charge

      t.timestamps
    end
  end
end
