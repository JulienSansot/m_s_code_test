require 'test_helper'

class DeliveryChargeRuleTest < ActiveSupport::TestCase

  test "below 50 pounds" do
    delivery_charge = DeliveryChargeRule.get_charge(DeliveryChargeRule.all.to_a, 30)
    assert_equal(4.95, delivery_charge.to_f)
  end

  test "50 pounds" do
    delivery_charge = DeliveryChargeRule.get_charge(DeliveryChargeRule.all.to_a, 50)
    assert_equal(2.95, delivery_charge.to_f)
  end

  test "between 50 and 90 pounds" do
    delivery_charge = DeliveryChargeRule.get_charge(DeliveryChargeRule.all.to_a, 70)
    assert_equal(2.95, delivery_charge.to_f)
  end

  test "90 pounds" do
    delivery_charge = DeliveryChargeRule.get_charge(DeliveryChargeRule.all.to_a, 90)
    assert_equal(0, delivery_charge.to_f)
  end

  test "above 90 pounds" do
    delivery_charge = DeliveryChargeRule.get_charge(DeliveryChargeRule.all.to_a, 150)
    assert_equal(0, delivery_charge.to_f)
  end

end
