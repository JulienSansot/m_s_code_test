require 'test_helper'

class BasketTest < ActiveSupport::TestCase

  test "No offer and first level of delivery charge" do
    basket = Basket.new(Product.all.to_a, DeliveryChargeRule.all.to_a, Offer.all.to_a)

    basket.add('S01')
    basket.add('B01')

    assert_equal(37.85, basket.total.to_f)
  end

  test "Jeans offer and first level of delivery charge" do
    basket = Basket.new(Product.all.to_a, DeliveryChargeRule.all.to_a, Offer.all.to_a)

    basket.add('J01')
    basket.add('J01')

    assert_equal(54.375, basket.total.to_f)
  end

  test "No offer and second level of delivery charge" do
    basket = Basket.new(Product.all.to_a, DeliveryChargeRule.all.to_a, Offer.all.to_a)

    basket.add('J01')
    basket.add('B01')

    assert_equal(60.85, basket.total.to_f)
  end

  test "Jeans offer and no delivery charge" do
    basket = Basket.new(Product.all.to_a, DeliveryChargeRule.all.to_a, Offer.all.to_a)

    basket.add('S01')
    basket.add('S01')
    basket.add('J01')
    basket.add('J01')
    basket.add('J01')

    assert_equal(98.275, basket.total.to_f)
  end

end
