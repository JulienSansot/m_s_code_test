require 'test_helper'

class OfferTest < ActiveSupport::TestCase

  test "No products match offers" do
    products = [
      Product.new(code: 'S01'),
      Product.new(code: 'B01')
    ]

    offer_discount = Offer.GetOfferDiscounts(Offer.all.to_a, products)

    assert_equal(0, offer_discount.to_f)
  end

  test "Only one pair of jeans so no discount" do
    products = [
      Product.new(code: 'J01', price: 32.95),
      Product.new(code: 'B01', price: 32.95)
    ]

    offer_discount = Offer.GetOfferDiscounts(Offer.all.to_a, products)

    assert_equal(0, offer_discount.to_f)
  end

  test "applying the BuyOneGetOneHalfPrice offer on jeans" do
    products = [
      Product.new(code: 'J01', price: 32.95),
      Product.new(code: 'J01', price: 32.95)
    ]

    offer_discount = Offer.GetOfferDiscounts(Offer.all.to_a, products)

    assert_equal(16.475, offer_discount.to_f)
  end

  test "applying the BuyOneGetOneHalfPrice offer on jeans, with 3 jeans" do
    products = [
      Product.new(code: 'J01', price: 32.95),
      Product.new(code: 'J01', price: 32.95),
      Product.new(code: 'J01', price: 32.95)
    ]

    offer_discount = Offer.GetOfferDiscounts(Offer.all.to_a, products)

    assert_equal(16.475, offer_discount.to_f)
  end

end
